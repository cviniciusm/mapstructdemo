package com.example.mapstructdemo;

import com.example.mapstructdemo.model.Person;
import com.example.mapstructdemo.model.SexEnum;
import com.example.mapstructdemo.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class MyApplicationRunner implements ApplicationRunner {

    @Autowired
    PersonRepository personRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Person person;

        try {
            person = new Person();
            person.setName("João");
            person.setBirthday(LocalDate.of(1995, 01, 10));
            person.setSex(SexEnum.M);
            personRepository.save(person);
            System.out.println(person);

            person = new Person();
            person.setName("Maria");
            person.setBirthday(LocalDate.of(1990, 10, 5));
            person.setSex(SexEnum.F);
            personRepository.save(person);
            System.out.println(person);
        } catch(java.time.DateTimeException e) {
            e.printStackTrace();
        }
    }

}
