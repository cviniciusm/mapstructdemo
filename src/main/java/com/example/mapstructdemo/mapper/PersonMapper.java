package com.example.mapstructdemo.mapper;

import com.example.mapstructdemo.dto.PersonDto;
import com.example.mapstructdemo.model.Person;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface PersonMapper {

    PersonDto mapToDto(Person person);

    Person mapToEntity(PersonDto personDto);

    List<PersonDto> mapToListDto(List<Person> person);

    List<Person> mapToListEntity(List<PersonDto> personDto);
}
