package com.example.mapstructdemo.repository;

import com.example.mapstructdemo.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Long> {}
