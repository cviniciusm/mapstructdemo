package com.example.mapstructdemo.model;

public enum SexEnum {

    M("M", "masculine"), F("F", "feminine");

    private String sexType;

    private String decription;

    SexEnum(String sexType, String decription) {
        this.sexType = sexType;
        this.decription = decription;
    }

    public String getSexType() {
        return sexType;
    }

    public void setSexType(String sexType) {
        this.sexType = sexType;
    }

    public String getDecription() {
        return decription;
    }

    public void setDecription(String decription) {
        this.decription = decription;
    }

}
