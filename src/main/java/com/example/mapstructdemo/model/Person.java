package com.example.mapstructdemo.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;
import java.time.Period;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Entity
public class Person extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private LocalDate birthday;

    private SexEnum sex;

    public Integer getAge() {
        Period diff= Period.between(this.birthday, LocalDate.now());

        return diff.getYears();
    }

}
