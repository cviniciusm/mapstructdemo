package com.example.mapstructdemo.model;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.math.NumberUtils;

public abstract class AbstractEntity<ID extends Comparable<ID>> implements Entity<ID> {

    private static final long serialVersionUID = 1L;

    @Override
    public abstract ID getId();

    @Override
    public abstract void setId(ID id);

    @Override
    public int compareTo(Entity<ID> otherEntity) {
        if (otherEntity == null) {
            return NumberUtils.INTEGER_ONE;
        } else {
            return new CompareToBuilder().append(getId(), otherEntity.getId()).toComparison();
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof AbstractEntity)) {
            return false;
        } else {
            return new EqualsBuilder().append(getId(), ((AbstractEntity<?>) obj).getId()).isEquals();
        }
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(11, 27).append(getId()).toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", getId()).toString();
    }

}
