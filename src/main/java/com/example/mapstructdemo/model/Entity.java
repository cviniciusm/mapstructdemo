package com.example.mapstructdemo.model;

import java.io.Serializable;

public interface Entity<ID extends Comparable<ID>> extends Comparable<Entity<ID>>, Serializable {

    ID getId();

    void setId(ID id);

}
