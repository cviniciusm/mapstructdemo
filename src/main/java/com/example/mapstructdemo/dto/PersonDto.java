package com.example.mapstructdemo.dto;

import com.example.mapstructdemo.model.SexEnum;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PersonDto {

    @Setter(AccessLevel.NONE)
    private Long id;

    private String name;

    private Date birthday;

    @Setter(AccessLevel.NONE)
    private Integer age;

    private SexEnum sex;

}
